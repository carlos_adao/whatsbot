var express = require('express');
const banco = require('./stages/banco');
const stages = require('./stages/stages');

var app = express();

app.use(express.json());
app.use(express.urlencoded({extended:false}));

const venom = require('venom-bot');

app.use(express.static("public"));

app.set('view engine', 'ejs');

app.get('/start', function(req, res){
    res.render("start");
});

app.get('/send', function(req, res){
    res.render("send");
});

//Função para avançar o estado o usuário
function getStage(user){
    return banco.db[user].stage;
}

console.log(stages.step[getStage("user1")].obj.execute());
console.log(stages.step[getStage("user2")].obj.execute());


app.get('/init', function(req, res){
    venom
    .create(
        'sessionName',
        (base64Qr, asciiQR, attempts, urlCode) => {
        //   console.log(asciiQR); // Optional to log the QR in the terminal
        var matches = base64Qr.match(/^data:([A-Za-z-+\/]+);base64,(.+)$/),
            response = {};

        if (matches.length !== 3) {
            return new Error('Invalid input string');
        }
        response.type = matches[1];
        response.data = new Buffer.from(matches[2], 'base64');

        var imageBuffer = response;
        require('fs').writeFile(
            'public/out.png',
            imageBuffer['data'],
            'binary',
            function (err) {
            if (err != null) {
                // console.log(err);
            }
            }
        );
        },
        undefined,
        { logQR: false }
    )
    .then((client) => {
        start(client);
    })
    .catch((erro) => {
        // console.log(erro);
    });

    function start(client) {
        client.onMessage((message) => {
            var msg =  stages.step[getStage(message.from)].obj.execute();
            client.sendText(message.from, msg)
            // if (message.body === 'Promo motoboy' && message.isGroupMsg === false) {
            //     var ipolicia = "🚔";
            //     var ib = "👩🏻‍🚒";
            //     var isamu = "🚑";
                
            //     var msg = `Olá obrigado por entrar em contato\nEsses São os seguintes resultados:\n Matheus Motoboy⭐⭐⭐ - 7399159-8948\nPaulo⭐⭐ - 7399159-8948\n Juliano⭐⭐⭐⭐⭐ - 73456878987`;
                
                
            // }else{
            //     var msg = "Olá Eu sou o 🛒PROMO SERVICE\n\nDesculpe não entendi😞\nPara que eu possa te ajudar,\ninforme alguma das opções abaixo\n\n\t  👇🏽\n\tMENU\n\n1 - Motoboy\n2 - Farmacias\n3 - Supermercados\n4 - Uber"
            // }

            // client.sendText(message.from, msg)
            // .then((result) => {
            //     console.log('Result: ', result); //return object success
            // }).catch((erro) => {
            //     //console.error('Error when sending: ', erro); //return object error
            // });
        });

        //Enviando mensagem para um numero especifico
        app.get('/message', function(req, res){
            var msg = `RESTAURANTE COCO DENDÊ\n👨🏾‍🍳 Olá Ricardo.\njose.carlos.adao@hotmail.com\n\nReserva confirmada para o dia: 14/05/2021 as 21:00h\nMesa reservada para: 3 pessoas\n\nCaso deseje escolher o cardápio\nclick no link abaixo.\n\t\t\t👇🏽\nhttp://reservas.cocodende.com.br/cardapio/choose/114`;
            var phone_robsao ="5573981792728@c.us";
            var phone_adao ="5573991598949@c.us";
            client.sendText(phone_adao, msg)
            res.json( { result: "Sucess", message: "mensagen Eviada" });
        });

        app.post("/message", async (req, res) => {
            
            var nome   = req.body.nome; 
            var email  = req.body.email;
            var tel    = req.body.tel;
            var data   = req.body.data;
            var hora   = req.body.hora;
            var url = req.body.url;
            var qtd_pessoas_mesa = req.body.qtd_pessoas_mesa;
            
            var msg = "RESTAURANTE COCO DENDÊ\n👨🏾‍🍳 Olá "+nome+".\n"+email+"\n\nReserva confirmada para o dia: "+data+" às "+hora+"\nMesa reservada para: "+qtd_pessoas_mesa+" pessoas\n\nCaso deseje escolher o cardápio\nclick no link abaixo.\n\t\t\t👇🏽\n"+url+"";

            client.sendText('55'+ tel + '@c.us', msg)
            res.json( { result: "Sucess", message: "mensagen Eviada" });
        });

        //Retornando os grupos
        app.get('/group', function(req, res){
            res.json(client.getAllChatsNewMsg());
        });
    }

    res.json( { result: "200", message: "Servidor iniciado" });
});

app.listen(3002,"localhost",()=>{
    console.log("Sevidor ON");
});




